import numpy as np
import imutils
import pickle
import cv2
import os
from imutils import paths

# Cargamos el modelo Caffe de deteccion de rostros
detector = cv2.dnn.readNetFromCaffe('bin/recursos/deploy.prototxt','bin/recursos/res10_300x300_ssd_iter_140000.caffemodel')


# Cargamos el modelo para detectar rasgos
rasgos = cv2.dnn.readNetFromTorch("bin/recursos/openface_nn4.small2.v1.t7")

# Cargamos las rutas de las imagenes

rutas_imagenes = list(paths.list_images("bin/dataset"))

# FIJAMOS EL VALOR UMBRAL DE VERIFICACIÓN SEGÚN LAS CURVAS ROC:

umbral = 0.69

def detectar(imagen):
	(h , w) = imagen.shape[:2]
	# Detectamos la cara en la imagen cargada
	# Para ello, construimos un blob con las caracteristicas del detector de rostros
	# de la imagen cargada

	blob_img = cv2.dnn.blobFromImage(cv2.resize(imagen, (300, 300)), 1.0, (300, 300), (104.0, 177.0, 123.0), swapRB = False, crop = False)
	
	# Ahora le pasamos el blob al detector para localizar las coordenadas donde se encuentra la cara

	detector.setInput(blob_img)
	detectado = detector.forward()

	# Con los datos obtenidos revisamos que sean rostros validos para asi 
	# poder trabajar con ellos

	if len(detectado) > 0:

		# Extremos la posicion con las coordenadas del sector de la foto en la que se encuentra la maxima probabilidad de tener un rostro en su 
		# interior
		max_prob = np.argmax(detectado[0, 0, :, 2])
		probabilidad = detectado[0, 0, max_prob, 2]

		# Si cumple una probabilidad mínima de que sea un rostro se procede a su procesamiento
		if probabilidad > 0.5:

			# Extraemos las coordenadas de la zona en la que se encuentra el rostro, para ello multiplicamos lo que nos devuelve la red con un vector de ceros 
			# de tipo entero para convertirlo a numeros enteros ya que la red nos lo devuelve en tipo char.
			(X, Y, W, H) = (detectado[0, 0, max_prob, 3:7] * np.array([w, h, w, h])).astype('int')

		else:
			# En el caso de no detectar nada ponemos un recuadro estandar para que no se produzcan errores
			(X, Y, W, H) = (0, 0, w, h)

	return X, Y, W, H

def gen_libreria():
	# Inicializamos las listas de rasgos y nombres de cada clase, se usa cada vez que añadimo un nuevo usuario al sistema de verificación
	rasgos_conocidos = []
	id_actual = 0
	dicc={}

	# Recorremos todo el directorio de imagenes de base de datos extrayendo sus vectores de caracteristicas y almacenandolo en archivos .pickle

	for (i, ruta_imagen) in enumerate(rutas_imagenes):
		nombre = ruta_imagen.split(os.path.sep)[-2]					# Extraemos el nombre de la clase que esta contenido en la carpeta donde se almacena la foto de la clase
		img = cv2.imread(ruta_imagen)								# Cargamos la imagen que vamos a analizar
		(X, Y, W, H) = detectar(img)								# Extraemos las coordenadas en las que se encuentra el rostro

		cara = img[Y:H, X:W]										# Guardamos a parte la region de la cara
		blob = cv2.dnn.blobFromImage(cara, 1.0/255, (96,96), (0,0,0), swapRB = True, crop = False)					# Creamos el blob de la cara para poder extraer sus caracterisiticas con la red neuronal
		rasgos.setInput(blob)																						# Extraemos los rasgos
		vector_rasgos = rasgos.forward()																			# Los almacenamos en el vector de 128-D

		# Añadimos a los vectores el nombre de la imagen leida y su vector de rasgos

		dicc[id_actual] = nombre
		id_actual += 1
		rasgos_conocidos.append(vector_rasgos.flatten())

	# Creamos un diccionario con cada nombre y su rasgo conocido
	base_datos = rasgos_conocidos

	# Guardando base de datos
	with open("bin/recursos/dicc.pickle", 'wb') as f:
		pickle.dump(dicc, f)

	with open("bin/recursos/base_datos.pickle", 'wb') as f:
		pickle.dump(base_datos, f)

	print('Base de Datos Actualizada')

# Función que usa la web cam para extraer el vector de características de la persona que presenta el documento y compararlo con el vector de referencia
# en la BBDD y en funcion a la distancia muestra por pantalla si es esa persona o no
def verificar(etiqueta):

	# Cargamos la base de datos
	with open("bin/recursos/base_datos.pickle", 'rb') as f:
		base_datos = pickle.load(f)
	with open("bin/recursos/dicc.pickle", 'rb') as f:
		dicc = pickle.load(f)
		dicc_inv = {v:k for k,v in dicc.items()}
		video = cv2.VideoCapture(0)

	# Extraemos el vector de caracteristicas de referencia

	try:
		id_referencia = dicc_inv[etiqueta]
		vector_referencia = base_datos[id_referencia]
	except:
		# Si no se encuentra la id en la base de datos se genera un vector de ceros, de esta forma nos aseguramos que la verificación saldrá negativa
		vector_referencia = np.zeros(128)
	
	while(True):
		# Leemos el frame de video que entra
		ret, frame = video.read()

		if ret == True:

			(X, Y, W, H) = detectar(frame)																	# Detectamos el rostro en el fram
			cara = frame[Y:H, X:W]																			# Recortamos el área en la que se encuentra dicho rostro
			(w, h) = cara.shape[:2]
			if w < 20 or h < 20:																			# Comprobamos que tenga unas dimensiones minimas para asi evitar falsas detecciones
				continue
			blob = cv2.dnn.blobFromImage(cara, 1.0/255, (96,96), (0,0,0), swapRB = True, crop = False)		# Generamos el blob de la imagen
			rasgos.setInput(blob)																			# Extraemos los rasgos
			vector_rasgos = rasgos.forward()																# Generamos el vector de 128-D

			distancia = np.linalg.norm(vector_referencia - vector_rasgos)									# Medimos la distancia entre ambos vectores
			if distancia < umbral:
				color = (0, 255, 0)																			# Si la distancia está por debajo del umbral el recuadro será VERDE
			else:
				color = (0, 0, 255)																			# Si la distancia está por encima del umbral el recuadro será ROJO

			grosor = 2
			cv2.rectangle(frame, (X, Y), (W, H), color, grosor)												# Dibujamos el rectángulo sobre el frame recibido

			cv2.imshow('WEBCAM', frame)																		# Mostramos los frames procesados por pantalla
			if cv2.waitKey(1) & 0xFF == ord('e'):
				break

	video.release()
	cv2.destroyAllWindows()
