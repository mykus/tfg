import sys
import os
from tensorflow.python.keras.preprocessing.image import ImageDataGenerator
from tensorflow.python.keras import optimizers
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers import Dropout, Flatten, Dense, Activation
from tensorflow.python.keras.layers import Convolution2D, MaxPooling2D
from tensorflow.python.keras import backend as K 


# Indicamos la ruta desde donde se van a cargar la base de datos
K.clear_session()
data_entrenamiento = './36clases/entrenamiento'
data_validacion = './36clases/validacion'

## Parámetros

epocas = 20						# Cantidad de épocas que va a tener el entrenamiento (veces que se va a pasar la BBDD entera)
altura, longitud = 32, 32		# Tamaño de las imágenes de entrada
batch_size = 32 				# Tamaño que va a tener el batch (cantidad de imágenes que se le manda a procesar en cada paso)
pasos = 1000 					# Cantidad de pasos que va a tener una época
pasos_validacion = 200			# Cantidad de imágenes que vamos a usar en cada validación
filtrosConv1 = 64				# Cantidad de filtros que va a tener cada filtro convolucional de la red
filtrosConv2 = 64
filtrosConv3 = 64
tamano_filtro1 = (5,5)			# Tamaño de los filtros en cada convolución
tamano_filtro2 = (3,3)
tamano_filtro3 = (3,3)
tamano_pool = (2,2)				# Tamaño que va a tener el filtro de pooling
clases = 36						# Cantidad de clases a entrenar
lr = 0.0005						# Learning rate asignado (a mas pequeño mas precision tendrá la red)

## Pre-Procesamiento de Imagenes

entrenamiento_datagen = ImageDataGenerator(rescale = 1./255,)		# Preprocesamiento que se le aplica a las imagenes de entrada y validación para que sean analizadas por la red
validacion_datagen = ImageDataGenerator(rescale = 1./255)			# en este caso solo les aplicamos un reescalado para que el valor de los pixeles estén entre 0 y 1 (normalizado)

imagen_entrenamiento = entrenamiento_datagen.flow_from_directory(	# Hacemos un generado de imagenes que va a acceder a las carpetas que le digamos y les aplicará un preprocesado indicado
	data_entrenamiento,												# directorio al que va a entrar
	target_size = (altura, longitud), 								# reescalado que se le va a aplicar
	batch_size = batch_size, 										# Cantidad de bloques que va a generar (bloques de 32 imagenes)
	class_mode = 'categorical'										# Indicamos que la clasificacion va a ser categórica debido a que tenemos diferentes clases, no es una red binaria
)

imagen_validacion = validacion_datagen.flow_from_directory(
	data_validacion,
	target_size = (altura, longitud),
	batch_size = batch_size,
	class_mode = 'categorical'
)


## Crear la red CNN

cnn = Sequential()									# Inicializamos la res

cnn.add(Convolution2D(filtrosConv1,					# Le añadimos la primera capa convolucional con una funcion de activación RELU
	tamano_filtro1,
	padding = 'same',
	input_shape = (altura, longitud, 3),
	activation = 'relu'))

cnn.add(MaxPooling2D(pool_size = tamano_pool))		# Le aplicamos una capa de MaxPooling para reducir el tamaño de las matrices

cnn.add(Convolution2D(filtrosConv2,					# Le añadimos la segunda capa convolucional con una funcion de activación RELU
	tamano_filtro2,
	padding = 'same',
	activation = 'relu'))

cnn.add(MaxPooling2D(pool_size = tamano_pool))		# Le aplicamos una capa de MaxPooling para reducir el tamaño de las matrices

cnn.add(Convolution2D(filtrosConv3,					# Le añadimos la tercera capa convolucional con una funcion de activación RELU
	tamano_filtro3,
	padding = 'same',
	activation = 'relu'))

cnn.add(MaxPooling2D(pool_size = tamano_pool))		# Le aplicamos una capa de MaxPooling para reducir el tamaño de las matrices

cnn.add(Flatten())									# Aplanamos toda la informacion contenida en la ultima matriz en un solo vector

cnn.add(Dense(256, activation = 'relu'))			# Mandamos el vector aplanado a una capa completamente conectada (Fully Connected)

cnn.add(Dropout(0.5))								# "Apagamos" la mitad de las neuronas de la capa densa anterior para evitar tener problemas de sobreentrenamiento (Overfiting)

cnn.add(Dense(clases, activation = 'softmax'))		# Pasamos a una capa densa que finalmente tendra la misma cantidad de neuronas que de clases y le aplicamos una función de activación 
													# softmax que nos dirá cual de todas las clases tiene la mayor probabilidad de que sea la clase de la imagen de entrada

cnn.compile(loss = 'categorical_crossentropy', 		# Finalmente cerramos la estructura de la red marcandole la funcion de perdida a utilizar "Entropia cruzada", el optimizador que vamos
	optimizer = optimizers.Adam(lr = lr), 			# a usar que va a ser de tipo "Adam" y le diremos que use como referencia para estos parametros la precisión de la red
	metrics = ['accuracy']
)

cnn.fit(imagen_entrenamiento, 						# Comenzamos el entrenamiento de la red y le pasamos los parametros fijados al principio de set de imágenes para entrenar, cuantas 
	steps_per_epoch = pasos, 						# epocas vamos a entrenar con cuantos pasos en cada época y las variables que contienen las imágenes de validacion y la cantidad 
	epochs = epocas, 								# de pasos de validación que queremos hacer
	validation_data = imagen_validacion, 
	validation_steps = pasos_validacion
)

## Guardamos el modelo

cnn.save('./modelo/jarvis.h5')
cnn.save_weights('./modelo/jpesos.h5')