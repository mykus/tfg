# CODIGO PARA ENTRENAR RED NEURONAL

1- Descomprimir uno de los tres .zip que se encuentran en la carpeta segun lo que se quiera obtener en el modelo

- 36clases.zip -> Red neuronal que identifique letras (A - Z) y numeros (0 - 9) 
- 26clases.zip -> Red neuronal que identifique letras (A - Z)
- 10clases.zip -> Red neuronal que identifique numeros (0 - 9)

2- Modificar en entrenar.py la carpeta donde se van a cargar la base de datos (lineas 13 y 14)

3- Compilar entrenar.py y esperar a que el proceso finalice

4- Los modelos quedaran guardados en la carpeta "modelo"