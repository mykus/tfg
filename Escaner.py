import numpy as np
import cv2
import copy
import os
from skimage.filters import threshold_local
import imutils
import OCR

def order_points(puntos):
	# Definims un vector de entrada para las coordenadas de los cuatro puntos del ROI que se esté analizando
	rectangulo = np.zeros((4, 2), dtype = "float32")

	# El punto de más arriba tendra la suma mas pequeña y el de mas abajo la
	# mas grande
	s = puntos.sum(axis = 1)
	rectangulo[0] = puntos[np.argmin(s)]
	rectangulo[2] = puntos[np.argmax(s)]

	# Ahora calculamos la diferencia para saber las esquinas de la izquierda
	# y de la derecha
	diff = np.diff(puntos, axis = 1)
	rectangulo[1] = puntos[np.argmin(diff)]
	rectangulo[3] = puntos[np.argmax(diff)]

	return rectangulo

def four_point_transformation(imagen, puntos):
	rectangulo = order_points(puntos)												# Ordenamos los puntos detectados
	(aizq, ader, bizq, bder) = rectangulo
	# Calculamos el ancho de la nueva imagen mediante el teorema de Pitágoras
	anchoA = np.sqrt(((bizq[0] - bder[0]) ** 2) + ((bizq[1] - bder[1]) ** 2))
	anchoB = np.sqrt(((ader[0] - aizq[0]) ** 2) + ((ader[1] - aizq[1]) ** 2))
	maxancho = max(int(anchoA), int(anchoB))										# Sacamos el ancho de la imagen
	# Calculamos el alto de la nueva imagen mediante el teorema de Pitágoras
	altoA = np.sqrt(((ader[0] - bizq[0]) ** 2) + ((ader[1] - bizq[1]) ** 2))
	altoB = np.sqrt(((aizq[0] - bder[0]) ** 2) + ((aizq[1] - bder[1]) ** 2))
	maxaltura = max(int(altoA), int(altoB))											# Sacamos el alto de la imagen

	# Con las medidas creamos una imagen nueva para almacenar la vieja
	hole = np.array([[0,0], [maxancho -1, 0], [maxancho -1, maxaltura -1], [0, maxaltura - 1]], dtype = "float32")

	# Generamos la matriz transformación 
	Mat_trans = cv2.getPerspectiveTransform(rectangulo, hole)

	# Aplicamos la transformación a la imagen original
	warped = cv2.warpPerspective(imagen, Mat_trans, (maxancho, maxaltura))

	return warped

# Función que se encarga de enviar una copia del ROI recortado a la red neuronal para la deteccion de los diferentes caracteres y su significado
def lec (x, y, w, h, im, tipo):
	aux = copy.copy(im[y:h, x:w])
	text = OCR.lectura(aux, tipo)	
	return text

# Función que se encarga de detectar en la imagen de entrada la presencia del DNI
def escanear(im):
	im_or = im.copy()																# Generamos una copia del la imagen de entrada para poder trabajar sobre ella
	ratio = im.shape[0] / 500.0														# Extraemos el ratio de la imagen en el caso de hacerle un resize a 500 pixeles
	im = imutils.resize(im, height = 500)
	gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)										# Cambiamos a blanco y negro
	gauss = cv2.GaussianBlur(gray, (5, 5), 0)										# Aplicamos el desenfoque Gaussiano
	cany = cv2.Canny(gauss, 75, 200)												# Hacemos el filtrado de Canny para detectar bordes
	cnt = cv2.findContours(cany.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)		# Buscamos los contornos en la imagen
	cnt = imutils.grab_contours(cnt)												# Ajustamos los contornos para que haya compatibilidad entre las librerias
	cnt = sorted(cnt, key = cv2.contourArea, reverse = True)[:5]					# Ordenamos los contornos de mas grandes a mas pequeños para detectar en cual esta el DNI
	for c in cnt:																	# Recorremos todos los contornos y buscamos el que tenga cuatro esquinas (Normalmente es el primero debido al sort anterior)
		perimetro = 0.02 * cv2.arcLength(c, True)									# Calculamos el perímetro
		aproximacion = cv2.approxPolyDP(c, perimetro, True)							# Ajustamos el contorno detectado aun mas a los bordes para asi conseguir más precisión
		if len(aproximacion) == 4:													# Comprobamos que la aproximacion tenga cuatro esquinas
			recorte = aproximacion
			break

	im_out = four_point_transformation(im_or, recorte.reshape(4, 2) * ratio)		# Con el contorno detectado hacemos la corrección de inclinación para asegurarnos de que la imagen está recta
	im_out = cv2.resize(im_out, (1100, 800))										# Hacemos la imagen mas grande para facilitar la lectura de los diferentes ROI

	# Con la imagen enderezada procedemos a marcar las localizaciones de los diferentes datos
	plantilla = copy.copy(im_out)
	# Inicializamos los vectores de las plantillas
	APE1 = ([431, 225, 671, 274], [306, 104, 715, 156])
	APE2 = ([431, 265, 671, 305], [306, 174, 715, 219])
	NOM = ([431, 342, 705, 385], [306, 239, 715, 285])
	SE = ([431, 419, 488, 458], [306, 307, 363, 350])
	FNAC = ([431, 492, 657, 543], [306, 375, 542, 418])
	FVEN = ([632, 564, 856, 616], [306, 508, 542, 552])
	ID = ([88, 706, 430, 774], [0, 728, 268, 784])
	ESP = ([31, 128, 309, 220])

	# Seleccionamos el tipo de DNI que vamos a analizar: 0 dni actual, 1 dni antiguo
	pais = lec(ESP[0], ESP[1], ESP[2], ESP[3], plantilla, 1)
	if pais is '':
		pais = '0'
	if pais[:3] == 'ESP':
		tipo = 1
	else:
		tipo = 0

	# Apellido 1
	apellido1 = lec(APE1[tipo][0], APE1[tipo][1], APE1[tipo][2], APE1[tipo][3], plantilla, 3)
	# Apellido 2
	apellido2 = lec(APE2[tipo][0], APE2[tipo][1], APE2[tipo][2], APE2[tipo][3], plantilla, 3)
	#Nombre
	nombre = lec(NOM[tipo][0], NOM[tipo][1], NOM[tipo][2], NOM[tipo][3], plantilla, 3)
	# Sexo
	sexo = lec(SE[tipo][0], SE[tipo][1], SE[tipo][2], SE[tipo][3], plantilla, 3)
	# Fecha de Nacimiento
	Fnaci = lec(FNAC[tipo][0], FNAC[tipo][1], FNAC[tipo][2], FNAC[tipo][3], plantilla, 2)
	# Fecha de validez
	Fvali = lec(FVEN[tipo][0], FVEN[tipo][1], FVEN[tipo][2], FVEN[tipo][3], plantilla, 2)
	# DNI
	dni = lec(ID[tipo][0], ID[tipo][1], ID[tipo][2], ID[tipo][3], plantilla, 1)

	print('Todo Cargado')
	return nombre, apellido1, apellido2, sexo, Fnaci, Fvali, dni