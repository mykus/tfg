import numpy as np 
import cv2
import os
from tensorflow.python.keras.preprocessing.image import load_img, img_to_array
from tensorflow.python.keras.models import load_model

# Definimos el tamaño que tienen que tener las imágenes para la red neuronal y definimos las rutas de las redes entrenadas
altura, longitud = 32, 32
modelo1 = 'bin/modelo/letras_y_numeros.h5'
pesos1 = 'bin/modelo/letras_y_numeros_pesos.h5'
modelo2 = 'bin/modelo/numeros.h5'
pesos2 = 'bin/modelo/numeros_pesos.h5'
modelo3 = 'bin/modelo/letras.h5'
pesos3 = 'bin/modelo/letras_pesos.h5'

# Cargamos los modelos entrenados para la red que lee letras y números
lyn=load_model(modelo1)
lyn.load_weights(pesos1)
dic1 = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

# Cargamos los modelos entrenados para la red que lee números
num=load_model(modelo2)
num.load_weights(pesos2)
dic2 = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']

# Cargamos los modelos entrenados para la red que lee letras
let=load_model(modelo3)
let.load_weights(pesos3)
dic3 = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

# Función que segun el tipo de datos que se le pasen usa una red u otra para reconocer el caracter
def predecir(imagen, tipo):
	x = load_img(imagen, target_size = (altura, longitud))
	x = img_to_array(x)
	x = np.expand_dims(x, axis = 0)
	if tipo == 1:
		arreglo = lyn.predict(x)			# Nos devuelve la matriz de resultados de la red
		resultado = arreglo[0]				# Cogemos el vector de probabilidades de la matriz
		respuesta = np.argmax(resultado)	# Sacamos la posición con mayor probabilidad	
		caracter = dic1[respuesta]			# Elegimos la etiqueta relacionada con esa posición
	
	if tipo == 2:
		arreglo = num.predict(x)
		resultado = arreglo[0]
		respuesta = np.argmax(resultado)
		caracter = dic2[respuesta]

	if tipo == 3:
		arreglo = let.predict(x)
		resultado = arreglo[0]
		respuesta = np.argmax(resultado)
		caracter = dic3[respuesta]

	return caracter

# Función que ordena los contornos de izquierda a derecha
def sort_contours(contornos):
	# Creamos un vector con las coordenadas (x, y, w, h) de cada contorno detectado en la imagen
	vect_posiciones = [cv2.boundingRect(c) for c in contornos]
	# Ordenamos de menor a mayor los contornos en funcion a sus valores en funcion al eje horizontal (de menor a mayor en posiciones de x)
	(contornos, vect_posiciones) = zip(*sorted(zip(contornos, vect_posiciones), key=lambda coord:coord[1][0], reverse= False))
	return contornos

# Funcion con la que cambiaos la escala de las imagenes recortadas al tamaño deseado conservando su factor de forma par aevitar deformaciones
def escala(im):
	altura, anchura = im.shape[:2]
	max_alt = 24
	fs = max_alt / float(altura)
	im = cv2.resize(im, None, fx = fs, fy = fs, interpolation = cv2.INTER_AREA)
	return im

# Función para expandir el recorte del ROI ya que muchas veces lo recortaba muy a ras del caracter y la red no es capaz de 
# distinguir el contenido del ROI. Se comprueba siempre que este aumento del recuadro del ROI no desborde la imagen
def expand(x, y, w, h, im, add = 3):
	h_max, w_max = im.shape[:2]
	if (y-add) < 0:
		Y = 0
	else:
		Y = y-add
	if (x-add) < 0:
		X = 0
	else:
		X = x-add
	if (y+h+add) > h_max:
		H = h_max
	else:
		H = y+h+add
	if (x+w+add) > w_max:
		W = w_max
	else:
		W = x+w+add
	return X, Y, W, H

# Función que procesa el ROI y se lo pasa a la red neuronal para que esta identifique de qué caracter se trata
def lectura(im, tipo):
	im_g = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)		# Cambio a blanco y negro
	im_gss = cv2.GaussianBlur(im_g, (5, 5), 0)		# Desenfoque Gaussiano
	im_bn = cv2.adaptiveThreshold(im_gss, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)		# Binarizamos la imagen para mejorar la lectura de caracteres
	im_b = cv2.adaptiveThreshold(im_gss, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 11, 2)		# Imagen binarizad invertida para mejorar detección de los caracteres
	(_, contornos,_) = cv2.findContours(im_b.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)				# Detectamos los caracteres
	contornos = sort_contours(contornos)																		# Ordenamos los contornos de los caracteres de izquierda a derecha
	text = ''			# Inicializamos el string del resultado
	for c in contornos:	# Recorremos contorno a contorno
		(x, y, w, h) = cv2.boundingRect(c)															# Sacamos las coordenadas del recuadro
		if w >= 5 and (h >= 20 and h <= 60):														# Comprobamos que tenga un tamaño mínimo para contener un caracter
			X, Y, W, H = expand(x, y, w, h, im)														# Ampliamos la ROI para mejorar la lectura
			recorte = im_bn[Y:H, X:W]																# Hacemos el recorte del ROI desado sobre la imagen invertida y binarizada
			recorte = escala(recorte)																# Escalamos el ROI al tamaño necesario para su procesado por la red															
			recorte = cv2.copyMakeBorder(recorte, 2, 2, 2, 2, cv2.BORDER_CONSTANT, value = 0)		# Le añadimos un borde negro para ayuda a la red a localizar el caracter
			cv2.imwrite('temp.png', recorte)														# Creamos una imagen temporal donde guardar el caracter a leer para que no haya
			res = predecir('temp.png', tipo)														# problemas al cargarlo dentro de la red
			os.remove('temp.png')																	# una vez cargado se destruye la imagen temporal
			text = text + res 																		# Añadimos al string resultado el caracter detectado
	return text