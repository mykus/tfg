# SISTEMA DE CONTROL DE ACCESO MEDIANTE TARJETA DE IDENTIFICACION Y DETECCION FACIAL

1- Ejecutar el archivo GUI.py para arrancar la interfaz grafica.

2- En la carpeta "bin/dataset/" se encuentran un conjunto de subcarpetas que forman
   la base de datos de rostros, añadir ahi las fotos de las nuevas personas y 
   colocarlas en carpetas separadas con el numero de DNI de las mismas.

3- Pulsar el boton "Actualizar Base de Datos" en el programa si se ha añadido alguna
   persona nueva a la base de datos.

4- Para el funcionamiento de la identificacion facial, comprobar que se tiene una
   webcam instalada y que funciona correctamente.

5- Tened en cuenta que la calidad de dicha webcam y la calidad de las fotos que se
   introduzcan en la base de datos, puede influenciar en el resultado de la verificacion.


by Jose Miguel Llaurado Fons - 27/05/2019 - San Vicente del Raspeig