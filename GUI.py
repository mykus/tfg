from tkinter import *
from tkinter import messagebox
import numpy as np
import cv2
import copy
import os
from tkinter import filedialog
from PIL import ImageTk, Image

# Importamos los bloques del software
import FaceID
import Escaner


mw = Tk()
mw.title("DNI Register")
mw.geometry('400x450')

# Función que actua al pulsar el botón de cargar el DNI, se encarga de preguntar al ususario que archivo quiere analizar y de modificar las etiquetas
# de la interfaz grafica para mostrar por pantalla los resultados obtenidos

def clicked():
	# Preguntamos al usuario que archivo quiere analizar
	file = filedialog.askopenfilename()
	# Mandamos el archivo a la funcion principal para que extraiga los datos
	(nombre, apellido1, apellido2, sexo, Fnaci, Fvali, nid) = Escaner.escanear(cv2.imread(file))
	nom2.configure(text = nombre)
	ape2.configure(text = apellido1 + ' ' + apellido2)
	nac2.configure(text = Fnaci[0:2] + '/' + Fnaci[2:4] + '/' + Fnaci[4:9])
	val2.configure(text = Fvali[0:2] + '/' + Fvali[2:4] + '/' + Fvali[4:9])
	sex2.configure(text = sexo)
	dni2.configure(text = nid)

	# Guardamos en un archivo la etiqueta del DNI detectada para asi poderla usar para verificar al usuario
	with open('bin/recursos/label.txt', 'w') as archivo:
		archivo.write(nid)


# Funcion que actua al pulsar el boton de verificar y se encarga de leer la etiqueta del usuario a verificar y llamar a la funcion que se encarga de ello
def apariencia():
	with open('bin/recursos/label.txt', 'r') as archivo:
		etiqueta = archivo.read()
	FaceID.verificar(etiqueta)

# Actualiza la libreria que se encarga de almacenar los vectores de identificacion de cada usuario, es util en el caso de añadir nuevos usuarios a la base de datos	
def actualizar():
	FaceID.gen_libreria()


# Configuracion de TKinter para la interfaz grafica basica

btn = Button(mw, text = "Cargar DNI", command = clicked, width = 20, height = 5)
btn.grid(column = 0, row = 0)

nom = Label(mw, text = 'Nombre', font = 'BOLD')
nom.grid(column = 1, row = 1)
nom2 = Label(mw, text = '')
nom2.grid(column = 1, row = 2)

ape = Label(mw, text = 'Apellidos', font = 'BOLD')
ape.grid(column = 1, row = 3)
ape2 = Label(mw, text = '')
ape2.grid(column = 1, row = 4)

nac = Label(mw, text = 'Fecha de Nacimiento', font = 'BOLD')
nac.grid(column = 1, row = 5)
nac2 = Label(mw, text = '')
nac2.grid(column = 1, row = 6)

val = Label(mw, text = 'Fecha de Validez', font = 'BOLD')
val.grid(column = 1, row = 7)
val2 = Label(mw, text = '')
val2.grid(column = 1, row = 8)

sex = Label(mw, text = 'Sexo', font = 'BOLD')
sex.grid(column = 0, row = 9)
sex2 = Label(mw, text = '')
sex2.grid(column = 0, row = 10)

dni = Label(mw, text = 'DNI', font = 'BOLD')
dni.grid(column = 1, row = 9)
dni2 = Label(mw, text = '')
dni2.grid(column = 1, row = 10)

btn = Button(mw, text = "Comprobar Apariencia", command = apariencia, width = 20, height = 5)
btn.grid(column = 1, row = 0)

btn = Button(mw, text = "Actualizar Base de Datos", command = actualizar, width = 20, height = 2)
btn.grid(column = 0, row = 1)

mw.mainloop()